# Liste des ressources

## Penser-Concevoir-Élaborer

- [Une fiche test du module 1](1_Penser-Concevoir-Elaborer/fiche_module1_shorau.md)
- [Une autre fiche test pour le tutoriel](1_Penser-Concevoir-Elaborer/test_shorau.md)

## Mettre-en-oeuvre-Animer

- [Une fiche test du module 2](2_Mettre-en-oeuvre-Animer/fiche_module2_shorau.md)

## Accompagner

- [Une fiche test du module 1](3_Accompagner/fiche_module3_shorau.md)


## Observer-Analyser-Évaluer

- [Une fiche test du module 1](4_Observer-Analyser-Evaluer\fiche_module4_shorau.md)
- [Fiche élève : coloration de graphes (activité débranchée)](4_Observer-Analyser-Evaluer/a_colorier_00.html.pdf)
